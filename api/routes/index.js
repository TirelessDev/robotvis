var express = require('express');
var router = express.Router();


var remoteCommunication = require('../controllers/remoteCommunication');

// remote communication over sockets
router.post('/sendCode', remoteCommunication.sendToRobot);

module.exports = router;
