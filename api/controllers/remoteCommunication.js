/**
 * Created by Baden on 9/17/2016.
 */


/** WebSocket BROWSER-SERVER communication **/
var webSockets = {};

/** WebSocket DEVICE-SERVER communication **/
var deviceSockets = {};


var udp = require('dgram');
// creating a udp server
var server = udp.createSocket('udp4');

let deviceInfo = {};


function sendToBrowsers(msg, deviceID) { // send a message to all browsers registered with a particular deviceID
    if (webSockets[deviceID] != undefined) {
        for (var socket of webSockets[deviceID]) {
            socket.emit('status', {message: msg});
        }
    }
}


function sendToDevice(flag, msg, deviceID) { // send a message to all browsers registered with a particular deviceID
    if (deviceSockets[deviceID] != undefined) {
        if (msg === undefined) {
            deviceSockets[deviceID].emit(flag);
        } else {
            deviceSockets[deviceID].emit(flag, msg);
        }
    }
}

global.ioON = function (socket) {
    //******** Notify of new connection ****************
    console.log("GLOBAL WebSocket opened");
    var address = socket.request.connection.remoteAddress;
    console.log('New connection from ' + address);
    var socketID = socket.id;

    var connectedDevice = '';
    var registeredID = '';

    socket.emit('req_id');

    //******* Event handler for webclient ID ************
    socket.on('res_id', function (data) {
        console.log(data);
        if (data.deviceID != undefined) {
            registeredID = data.deviceID;
            if (webSockets[registeredID] == undefined) {
                webSockets[registeredID] = [socket];
            } else {
                webSockets[registeredID].push(socket);
            }
            if (deviceSockets[registeredID]) {
                sendToBrowsers("Device online", registeredID);
            } else {
                sendToBrowsers("Device offline", registeredID);
            }
            console.log(webSockets[registeredID].length);
        }
    });

    //******* Event handler for device ID ************
    socket.on('device_id', function (data) {
        console.log(data);
        if (data.deviceID !== undefined) {
            registeredID = data.deviceID;
            deviceSockets[registeredID] = socket;
            if (connectedDevice === '') {
                connectedDevice = registeredID;
            }
        }
    });


    socket.on('leg', function (data) {

        let staticIP = "192.168.88.253";
        let staticPort = 8888;
        server.send(JSON.stringify(data),staticPort,staticIP ,function(error){
            if(!error){
                //console.log('Data sent !!!');
            }else{console.log(error)}
        });

        // console.log("SendingData");
        //sendToDevice('leg_BC', {data: data},'hello');
        if(deviceInfo.port !== undefined && deviceInfo.address !== undefined){
            console.log(JSON.stringify(data));

        }
        //global.io.emit('leg_BC', {data: data});
    });
    socket.on('imu', function (data) {
        //sendToBrowsers(data,'hello');
        //global.io.emit('stdout', {data:data,robotID:robotID});
    });

    socket.on('disconnect', function () {
        if (connectedDevice !== '') { // If the current socket is to a connected Device
            delete deviceSockets[connectedDevice];
            console.log("Device disconnected: " + connectedDevice);
            sendToBrowsers("Device offline", registeredID);
        } else if (webSockets[registeredID]) { // Or if the current socket is to a connected Website we need to find which one and remove it from the array
            console.log("Searching for " + socketID);
            for (var i = 0; i < webSockets[registeredID].length; i++) {
                console.log("is this it: " + webSockets[registeredID][i].id);
                if (webSockets[registeredID][i].id === socketID) {
                    console.log("removed " + webSockets[registeredID].splice(i, 1)[0].id);
                }
            }
            console.log(webSockets[registeredID].length);
        }
        console.log("connection closed");
    });
};



// --------------------creating a udp server --------------------



// emits when any error occurs
server.on('error',function(error){
    console.log('Error: ' + error);
    server.close();
});

// emits on new datagram msg
server.on('message',function(msg,info){
	try{
		let data = JSON.parse(msg.toString());
		//console.log(data);2
		//global.io.emit('imu', data);
		deviceInfo = info;
	}catch(error){}
});

//emits when socket is ready and listening for datagram msgs
server.on('listening',function(){
    var address = server.address();
    var port = address.port;
    var family = address.family;
    var ipaddr = address.address;
    console.log('Server is listening at port' + port);
    console.log('Server ip :' + ipaddr);
    console.log('Server is IP4/IP6 : ' + family);
});

//emits after the socket is closed using socket.close();
server.on('close',function(){
    console.log('Socket is closed !');
});
server.bind(3002);

// setTimeout(function(){
//     server.close();
// },8000);




module.exports.sendToRobot = function (req, res) {

};
