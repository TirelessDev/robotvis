const Femur = 75;
const Tibia = 101;
const Coxia = 55;
const FemurOffset = 11.67;
const TibiaOffset = 78.33;
const mx12_resolution = 1;//11.375;
const ax12_resolution = 1;//3.41;
const coxiaResolution = mx12_resolution;
const femurResolution = ax12_resolution;
const tibiaResolution = ax12_resolution;

var container;

var camera, cameraTarget, scene, renderer;
var composer, renderPass, saoPass, copyPass;
var cameraRotation = 2;
var cameraHeight = 2;

var tickRate = 50; //update rate of the robot cmd process
var comRate = 50;
var textRenderRate = 100;

var state = "";

function Point(x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;
    var that = this;
    this.invert = function () {
        return new Point(that.x * -1, that.y * -1, that.z * -1)
    };
    this.add = function (point) {
        that.x = that.x + point.x;
        that.y = that.y + point.y;
        that.z = that.z + point.z;
    };
    this.sub = function (point) {
        that.x = that.x - point.x;
        that.y = that.y - point.y;
        that.z = that.z - point.z;
    };
}

var numID = function () { // generates a unique number for an ID
    var str = Math.random().toString(36).substr(2, 9);
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return Math.abs(hash);
};


function MountPoint(x, y, z, oriX, oriY, oriZ, part) {
    this.offset = new Point(x, y, z);
    this.orientation = new Point(oriX, oriY, oriZ);
    this.connectedPart = part;
    var that = this;
    this.connectPart = function (part) {
        that.connectedPart = part;
    }
}

function Part(meshPath) {
    this.mountPoints = [];
    this.meshPath = meshPath;
    this.type = "Part";
    this.mass = 1;
    var that = this;

    this.connectPart = function (x, y, z, oriX, oriY, oriZ, part) {
        part.connectedAt = new Point(x, y, z);
        that.mountPoints.push(new MountPoint(x, y, z, oriX, oriY, oriZ, part));
    };
}

function Command(type, duration, data) {
    this.id = numID();
    this.type = type;
    this.duration = duration;
    this.state = 0; // 0 = new, 1 = in progress, 2 = finished, -1 = error

    if (this.type === "legMovement") {
        let id = data.id;
        let start = data.start;
        let end = data.end;
        let height = data.height;
        let parent = this;
        this.data = new LegMovement(id, start, end, height, parent)
    } else {
        let start = data.start;
        let end = data.end;
        let height = data.height;
        let parent = this;
        this.data = new BodyMovement(start, end, height, parent);
    }

}

function LegMovement(id, start, end, height, parent) {
    this.legID = id;
    this.start = start;
    this.end = end;
    this.height = height;
    this.progress = 0;
    this.curve = new THREE.CatmullRomCurve3([
        new THREE.Vector3(start.x, start.y, start.z),
        new THREE.Vector3((start.x + end.x) / 2, (start.y + end.y) / 2, ((start.z + end.z) / 2) + height),
        new THREE.Vector3(end.x, end.y, end.z)
    ]);
    this.points = this.curve.getPoints(parent.duration / tickRate - 1);
}

function BodyMovement(start, end, height, parent) {
    this.start = start;
    this.end = end;
    this.height = height;
    this.progress = 0;
    this.curve = new THREE.CatmullRomCurve3([
        new THREE.Vector3(start.x, start.y, start.z),
        new THREE.Vector3((start.x + end.x) / 2, (start.y + end.y) / 2, ((start.z + end.z) / 2) + height),
        new THREE.Vector3(end.x, end.y, end.z)
    ]);
    this.points = this.curve.getPoints(parent.duration / tickRate - 1);
}

function CommandList() {
    this.commands = [];
    this.deleted = [];
    this.curveObjects = {};

    var that = this;
    this.processCommands = function () {
        if (state === "loaded") {
            let revisedCommands = [];
            for (let i = 0; i < that.commands.length; i++) { // check for finished commands
                let command = that.commands[i];
                if (command.state === 2) {
                    if (command.type === "legMovement" && that.curveObjects[command.id] !== undefined) {
                        that.curveObjects[command.id] = undefined;
                    }
                    that.deleted.push(command);
                } else {
                    revisedCommands.push(command);
                }
            }
            that.commands = revisedCommands;
            for (let i = 0; i < that.commands.length; i++) {
                let command = that.commands[i];
                if (command.state === 0) { // If this is the first time we are seeing the command.

                    if (command.type === "legMovement") { // If the command is a legMovement command
                        let points = [];
                        for (let i = 0; i < command.data.points.length; i++) {
                            points[i] = new Point(command.data.points[i].x, command.data.points[i].y, command.data.points[i].z);
                            points[i].add(legs[command.data.legID].mountPoint.offset);
                            points[i] = mmToScene(points[i]);
                        }
                        let geometry = new THREE.BufferGeometry().setFromPoints(points);
                        let material = new THREE.LineBasicMaterial({color: 0xff0000});
                        let curveObject = new THREE.Line(geometry, material);
                        scene.add(curveObject);
                        that.curveObjects[command.id] = curveObject;

                    }else if(command.type === 'bodyMovement'){

                    }
                    command.state = 1; //change the command state to "in progress"
                }
                if (command.state === 1) { // If we need to act on this
                    if (command.type === "legMovement") { // If the command is a legMovement command
                        let points = command.data.points;
                        let progress = command.data.progress;
                        if (progress >= points.length) { // if the command is complete change its state
                            command.state = 2;
                        } else {
                            let legID = command.data.legID;
                            legs[legID].localFootLocation.x = points[progress].x;
                            legs[legID].localFootLocation.y = points[progress].y;
                            legs[legID].localFootLocation.z = points[progress].z;
                            command.data.progress = progress + 1;
                            console.log(progress);
                            updateIK();
                        }
                    }else if(command.type === 'bodyMovement'){
                        let points = command.data.points;
                        let progress = command.data.progress;
                        if (progress >= points.length) { // if the command is complete change its state
                            command.state = 2;
                        } else {
                            bodyLocation.x = points[progress].x;
                            bodyLocation.y = points[progress].y;
                            bodyLocation.z = points[progress].z;
                            command.data.progress = progress + 1;
                            console.log(progress);
                            updateIK();
                        }
                    }
                }
            }
        }
    };

    // Leg movement commands
    this.moveLegFromTo = function (legID, start, height, end, duration) {
        let newCommand = new Command("legMovement", duration, {id: legID, start: start, end: end, height: height});
        that.commands.push(newCommand);
        return newCommand.id;
    };
    this.moveLegTo = function (legID, height, end, duration) {
        let start = clone(legs[legID].localFootLocation);
        let newCommand = new Command("legMovement", duration, {id: legID, start: start, end: end, height: height});
        that.commands.push(newCommand);
        return newCommand.id;
    };
    this.moveLegRelativeTo = function (legID, height, delta, duration) {
        let footLoc = new Point(legs[legID].localFootLocation.x, legs[legID].localFootLocation.y, legs[legID].localFootLocation.z);
        //footLoc.add(legs[legID].mountPoint.offset);
        let start = new Point(footLoc.x, footLoc.y, footLoc.z);
        let end = new Point(footLoc.x, footLoc.y, footLoc.z);
        end.add(delta);
        let newCommand = new Command("legMovement", duration, {id: legID, start: start, end: end, height: height});
        that.commands.push(newCommand);
        return newCommand.id;
    };

    // Body movement commands
    this.moveBodyRelativeTo = function (height, delta, duration) {
        let bodyLoc = new Point(bodyLocation.x, bodyLocation.y, bodyLocation.z);

        let start = new Point(bodyLoc.x, bodyLoc.y, bodyLoc.z);
        let end = new Point(bodyLoc.x, bodyLoc.y, bodyLoc.z);
        end.add(delta);

        let newCommand = new Command("bodyMovement", duration, {start: start, end: end, height: height});
        that.commands.push(newCommand);
        return newCommand.id;
    };
}


function Leg(localPosition, mountPoint) {
    //localPosition.add(mountPoint.offset);
    this.localFootLocation = localPosition;
    this.mountPoint = mountPoint;
    this.orientation = mountPoint.orientation;
    this.partLink = mountPoint.connectedPart;
}

var body = new Part("./models/body.stl");
body.mass = 4;
var hip = new Part("./models/hip.stl");
var femur = new Part("./models/femur.stl");
var foot = new Part("./models/foot.stl");


var angles = LegIK(new Point(150, 0, -25));
console.log(angles);


femur.connectPart(73.42, 0, -15.16, 0, angles.beta, 0, foot);
hip.connectPart(55, 0, 0, 0, angles.a, 0, femur);
body.connectPart(74, 0, 24, 0, 0, angles.gamma, hip);
body.connectPart(0, 74, 24, 0, 0, Math.PI / 2 + angles.gamma, clone(hip));
body.connectPart(0, -74, 24, 0, 0, -Math.PI / 2 + angles.gamma, clone(hip));
body.connectPart(-74, 0, 24, 0, 0, Math.PI + angles.gamma, clone(hip));

// body.mountPoints[0].connectPart(hip);
console.log(body);

var legs = [];
legs[0] = new Leg(new Point(130, 0, -24), body.mountPoints[0]);
legs[1] = new Leg(new Point(0, 130, -25), body.mountPoints[1]);
legs[2] = new Leg(new Point(0, -130, -25), body.mountPoints[2]);
legs[3] = new Leg(new Point(-130, 0, -25), body.mountPoints[3]);

console.log(legs);

var bodyLocation = new Point(0, 0, 0);

var axisHelper = {};

// socket = io.connect('http://dev.flowbox.co.nz:3002');
let socket = io.connect('http://localhost:3002');
socket.on('imu', function (data) {
    //console.log(data);
});

init();
let commandBuffer = new CommandList();

//commandBuffer.moveLegFromTo(0, new Point(0,0,0), 20, new Point(130,0,0),2000);
// commandBuffer.moveLegTo(0, 20, new Point(100, -100, -24), 2000);
// commandBuffer.moveLegRelativeTo(3, 20, new Point(-30, 100, 0), 500);
// commandBuffer.moveLegRelativeTo(1, 50, new Point(20, 20, 0), 1000);
// commandBuffer.processCommands();
// console.log(commandBuffer.commands);

setInterval(function () {
    commandBuffer.processCommands();
}, tickRate);


var font_helvetiker = {};
var font_state = "";

function TextList() {
    this.textObjects = [];

    this.update = function () {
        for(let i = 0; i<this.textObjects.length;i++){
            let attachedMesh = this.textObjects[i].attached.partLink.mesh;
            let position = new THREE.Vector3();
            position.setFromMatrixPosition( attachedMesh.matrixWorld );
            let offset = this.textObjects[i].offset;
            this.textObjects[i].position.set(offset.x +position.x,offset.y+position.y,offset.z+position.z);
            this.textObjects[i].quaternion.copy(camera.quaternion);
        }
    };
    this.addText = function(value, location, attachedTo) {
        let message = "";
        if(typeof value === "number"){
            message = value.toString();
        }else if(typeof value === "string"){
            message = value;
        }

        if (font_state === "loaded") {
            let xMid, text;
            let textShape = new THREE.BufferGeometry();
            let color = '#000000';
            let matLite = new THREE.MeshBasicMaterial({
                color: color,
                transparent: true,
                opacity: 1,
                side: THREE.DoubleSide
            });
            let shapes = font_helvetiker.generateShapes(message, 0.1, 2);
            let geometry = new THREE.ShapeGeometry(shapes);
            geometry.computeBoundingBox();
            xMid = -0.5 * (geometry.boundingBox.max.x - geometry.boundingBox.min.x);
            geometry.translate(xMid, 0, 0);
            textShape.fromGeometry(geometry);
            text = new THREE.Mesh(textShape, matLite);
            text.offset = location;
            text.attached = attachedTo;
            text.value = value;
            text.bufferValue = value; // bufferValue is used to buffer anyText changes, if an external processes wants to change the text value then it changes buffer and when the periodic re render is requested it will get rendered
            //text.id = numID();
            this.textObjects.push(text);
            scene.add(text);
            return text;
        }
    };
    this.changeText = function(value, text) {
        text.bufferValue = value;
    };
    this.reRender = function(){
        for(let i = 0; i<this.textObjects.length;i++){
            let text = this.textObjects[i];
            if(text.bufferValue !== text.value){
                let value = text.bufferValue;
                text.value = text.bufferValue;
                requestAnimationFrame(function () {
                    let message = "";
                    if(typeof value === "number"){
                        message = value.toString();
                    }else if(typeof value === "string"){
                        message = value;
                    }
                    let shapes = font_helvetiker.generateShapes(message, 0.1, 1);
                    let geometry = new THREE.ShapeGeometry(shapes);
                    geometry.computeBoundingBox();
                    let xMid = -0.5 * (geometry.boundingBox.max.x - geometry.boundingBox.min.x);
                    geometry.translate(xMid, 0, 0);
                    let textShape = new THREE.BufferGeometry();
                    textShape.fromGeometry(geometry);
                    text.geometry = textShape;
                });


            }
        }
    };
    this.removeText = function(textObj){
        let index = this.textObjects.indexOf(textObj);
        if(index !== -1){
            this.textObjects.splice(index,1);
            scene.remove(textObj);
        }
    }
}
const textList = new TextList();

setInterval(function () {
    textList.reRender();
}, textRenderRate);

function init() {

    container = document.createElement('div');

    document.body.appendChild(container);
    container.style.display = "flex";
    camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 1, 200);
    camera.position.set(6, 6, 2);
    camera.up.set(0, 0, 1);
    cameraTarget = new THREE.Vector3(0, 0, 0);

    scene = new THREE.Scene();
    //scene.fog = new THREE.Fog( '#72645b', 10, 20 );
    scene.fog = new THREE.Fog('#969aac', 10, 20);


    var font_loader = new THREE.FontLoader();
    font_loader.load('./fonts/helvetiker_regular.typeface.json', function (font) {
        font_state = "loaded";
        font_helvetiker = font;
        console.log("font_helvetiker loaded.")
    }); //end load function


    // Ground

    var plane = new THREE.Mesh(
        new THREE.PlaneBufferGeometry(100, 100),
        new THREE.MeshPhongMaterial({color: '#b0b3c8', specular: '#101010'})
    );
    // plane.rotation.x = -Math.PI/2;
    //plane.position.y = -0.5;
    scene.add(plane);

    plane.receiveShadow = true;

    var loader = new THREE.STLLoader();
    var geometryIndexedObjects = [];

    function loadParts(_Part, parentMesh) {
        var mesh;
        if (geometryIndexedObjects[_Part.meshPath] !== undefined) {
            var geometry = geometryIndexedObjects[_Part.meshPath];
            //var material = new THREE.MeshPhongMaterial( { color: 0xff5533, specular: 0x111111, shininess: 200 } );
            //var material = new THREE.MeshStandardMaterial({ color: '#ff5533'});
            var material = new THREE.MeshStandardMaterial({color: '#d4cfc9'});
            //material.roughness = 0.5 * Math.random() + 0.25;
            material.roughness = 0.7;
            material.metalness = 0.2;
            //material.color.setHSL( Math.random(), 1.0, 0.3 );
            mesh = new THREE.Mesh(geometry, material);
            mesh.position.set(0, 0, 0);
            // mesh.rotation.set( 0, - Math.PI / 2, 0 );
            // mesh.scale.set( 0.5, 0.5, 0.5 );

            mesh.castShadow = true;
            mesh.receiveShadow = true;
            _Part.mesh = mesh;
            parentMesh.add(mesh);
        }
        if (_Part.mountPoints.length !== 0) {
            for (var i = 0; i < _Part.mountPoints.length; i++) {
                loadParts(_Part.mountPoints[i].connectedPart, mesh);
                var offset = mmToScene(_Part.mountPoints[i].offset);
                var orientation = _Part.mountPoints[i].orientation;
                _Part.mountPoints[i].connectedPart.mesh.position.set(offset.x, offset.y, offset.z);
                _Part.mountPoints[i].connectedPart.mesh.rotation.set(orientation.x, orientation.y, orientation.z);
            }
        }
    }

    function geoCallback() {
        console.log("Geometry loaded, loading parts...");
        loadParts(body, scene);
        console.log("Parts loaded!");
        state = "loaded";

        legs[0].textLabel = textList.addText("0",new Point(0,0,0.5),legs[0]);
        legs[1].textLabel = textList.addText("1",new Point(0,0,0.5),legs[1]);
        legs[2].textLabel = textList.addText("2",new Point(0,0,0.5),legs[2]);
        legs[3].textLabel = textList.addText("3",new Point(0,0,0.5),legs[3]);

        body.mesh.position.z = 0;

        requestAnimationFrame(animate);
        //body.mesh.rotation.set( 0, - Math.PI / 6, 0 );
    }

    // ASCII file
    var geometryList = [];

    function loadGeometry(Part) {
        if (geometryList.indexOf(Part.meshPath) === -1) {
            geometryList.push(Part.meshPath);
            loader.load(Part.meshPath, function (geometry) {

                geometryIndexedObjects[Part.meshPath] = geometry;
                console.log("Loaded: " + Part.meshPath);
                if (geometryList.length === Object.keys(geometryIndexedObjects).length) {
                    geoCallback();
                }
            });
        }
        if (Part.mountPoints.length !== 0) {
            for (var i = 0; i < Part.mountPoints.length; i++) {
                loadGeometry(Part.mountPoints[i].connectedPart);
            }
        }
    }

    loadGeometry(body);

    // Lights

    var light = new THREE.PointLight('#ddffdd', 0.3);
    light.position.z = 70;
    light.position.y = -70;
    light.position.x = -70;
    scene.add(light);

    var light2 = new THREE.PointLight('#ffdbd2', 0.5);
    light2.position.z = 70;
    light2.position.x = -70;
    light2.position.y = 70;
    scene.add(light2);

    var light3 = new THREE.PointLight('#c8ddff', 0.5);
    light3.position.z = 70;
    light3.position.x = 70;
    light3.position.y = -70;
    scene.add(light3);

    //scene.add( new THREE.HemisphereLight( 0x443333, 0x111122 ) );
    var light4 = new THREE.AmbientLight('#ffffff', 0.2);
    scene.add(light4);
    //addShadowedLight( 5, 5, 5, 0xffffff, 1);
    //addShadowedLight( 0.5, 1, -1, 0xffaa00, 1 );
    // renderer

    var spotLight = new THREE.SpotLight('#ffffff', 1.2, 50, Math.PI / 4, 1, 2);
    spotLight.position.set(0, 0, 3);
    spotLight.target.position.set(0, 0, 0);
    spotLight.castShadow = true;
    spotLight.shadow.mapSize.width = 1024 * 0.2;
    spotLight.shadow.mapSize.height = 1024 * 0.2;

    scene.add(spotLight);
    scene.add(spotLight.target);
    axisHelper = new THREE.AxesHelper(1);

    scene.add(axisHelper);

    renderer = new THREE.WebGLRenderer({antialias: false});
    renderer.setClearColor(scene.fog.color);
    //renderer.setClearColor( 0xa0a0a0 );
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);

    renderer.gammaInput = true;
    renderer.gammaOutput = true;

    renderer.shadowMap.enabled = true;
    //renderer.shadowMap.renderReverseSided = true;
    //renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    container.appendChild(renderer.domElement);

    composer = new THREE.EffectComposer(renderer);
    renderPass = new THREE.RenderPass(scene, camera);
    renderPass.renderToScreen = true;
    composer.addPass(renderPass);

    saoPass = new THREE.SAOPass(scene, camera, false, true);


    saoPass.params = {
        output: 0,
        saoBias: 0,
        saoIntensity: 0.18,
        saoScale: 50,
        saoKernelRadius: 100,
        saoMinResolution: 0,
        saoBlur: true,
        saoBlurRadius: 8,
        saoBlurStdDev: 4,
        saoBlurDepthCutoff: 0.001
    };


    saoPass.renderToScreen = false;
    //composer.addPass( saoPass );

    // effectFXAA = new THREE.ShaderPass( THREE.FXAAShader );
    // effectFXAA.renderToScreen = true;
    // effectFXAA.uniforms[ 'resolution' ].value.set( 1 /  window.innerWidth , 1 / window.innerHeight );

    //composer.addPass( effectFXAA );


    window.addEventListener('resize', onWindowResize, false);

}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    composer.setSize(window.innerWidth, window.innerHeight);
}

var legState;
var lastGamepad;
var delta = 0;

setInterval(function () {
    if(legState){
        socket.emit("leg", legState);
    }
    // if(legState !== undefined && delta > 0.0001){
    //     //console.log(legState);
    //     socket.emit("leg",legState);
    // }
}, comRate);

var motorMap = [ // Maps the virtual to the real
    {//Leg 0
        a: {id: 2, maxDeg: 90, minDeg: -90, rangeDeg: 300, rangeDigital: 1023, invert: true},
        b: {id: 3, maxDeg: 90, minDeg: -90, rangeDeg: 300, rangeDigital: 1023, invert: true},
        g: {id: 1, maxDeg: 90, minDeg: -90, rangeDeg: 300, rangeDigital: 1023, invert: false, offset: 0}
    },

    {//Leg 1
        a: {id: 12, maxDeg: 90, minDeg: -90, rangeDeg: 300, rangeDigital: 1023, invert: true},
        b: {id: 13, maxDeg: 90, minDeg: -90, rangeDeg: 300, rangeDigital: 1023, invert: true},
        g: {id: 11, maxDeg: 90, minDeg: -90, rangeDeg: 300, rangeDigital: 1023, invert: false, offset: -90}
    },

    {//Leg 2
        a: {id: 22, maxDeg: 90, minDeg: -90, rangeDeg: 300, rangeDigital: 1023, invert: true},
        b: {id: 23, maxDeg: 90, minDeg: -90, rangeDeg: 300, rangeDigital: 1023, invert: true},
        g: {id: 21, maxDeg: 90, minDeg: -90, rangeDeg: 300, rangeDigital: 1023, invert: false, offset: 90}
    },

    {//Leg 3
        a: {id: 32, maxDeg: 90, minDeg: -90, rangeDeg: 300, rangeDigital: 1023, invert: true},
        b: {id: 33, maxDeg: 90, minDeg: -90, rangeDeg: 300, rangeDigital: 1023, invert: true},
        g: {id: 31, maxDeg: 90, minDeg: -90, rangeDeg: 300, rangeDigital: 1023, invert: false, offset: -180}
    }
];

function toMotorPos(legIndex, angleType, angle) {
    var mapping = motorMap[legIndex][angleType];

    if (mapping.invert) angle = angle * -1;

    angle = angle * 180 / Math.PI; // convert to deg
    if (mapping.offset !== undefined) angle += mapping.offset;

    if (mapping.id === 31) { // due to the 180 degree rotation, the gamma angle of leg 3 flips between -180 and 180
        if (angle < -180) {
            angle = 360 + angle
        }
    }

    // Check limits
    if (angle > mapping.maxDeg) angle = mapping.maxDeg;
    if (angle < mapping.minDeg) angle = mapping.minDeg;

    // Convert to motor position
    angle += mapping.rangeDeg / 2;
    angle = (angle / mapping.rangeDeg) * mapping.rangeDigital;

    return {id: mapping.id, ang: angle.toFixed(0)}; // round and return
}

function updateIK() { //robot specific
    legState = [];
    if (legs.length !== 0) {
        for (let i = 0; i < legs.length; i++) {
            //console.log(bodyLocation);
            legs[i].localFootLocation.sub(bodyLocation);
            // var rotatedPoint = rotate(legs[i].localFootLocation,legs[i].orientation.invert());
            var angles = LegIK(legs[i].localFootLocation);

            legState.push(toMotorPos(i, 'a', angles.a));
            legState.push(toMotorPos(i, 'g', angles.gamma));
            legState.push(toMotorPos(i, 'b', angles.beta));

            // if(i === 0){
            //     console.log(legState[i.toString()].a);
            // }
            // rotatedPoint = rotate(legs[i].localFootLocation,legs[i].orientation);
            //console.log(radToDeg(angles.a),radToDeg(angles.gamma),radToDeg(angles.beta));
            //console.log(body.mountPoints[i].orientation.z);

            if (isNaN(angles.a) || isNaN(angles.gamma) || isNaN(angles.beta)) {
                legs[i].localFootLocation.add(bodyLocation);
            } else {
                legs[i].localFootLocation.add(bodyLocation);
                textList.changeText(angles.gamma.toFixed(2), legs[i].textLabel);

                var _hip = legs[i].partLink;
                var _femur = _hip.mountPoints[0].connectedPart;
                var _foot = _femur.mountPoints[0].connectedPart;

                _hip.mesh.rotation.z = angles.gamma;
                _femur.mesh.rotation.y = angles.a;
                _foot.mesh.rotation.y = angles.beta;
            }

        }
    }
    let bodyLocationScene = mmToScene(bodyLocation);
    body.mesh.position.z = Math.min(1, Math.max(-0.05,bodyLocationScene.z));
    if (body.mesh.position.z > -0.05) {
        body.mesh.position.x = bodyLocationScene.x;
        body.mesh.position.y = bodyLocationScene.y;
    }
    axisHelper.position.copy(calculateCoM());
}

console.log(navigator.getGamepads());

function calculateCoM() {

    // var vector = new THREE.Vector3();
    // vector.setFromMatrixPosition( part.mesh.matrixWorld ); // get current mesh's absolute position in the world
    //vector.multiplyScalar(part.mass);
    var parts = [];

    function recurseAndAdd(part) {
        parts.push(part);
        var mountPoints = part.mountPoints;
        if (mountPoints.length > 0) {
            for (var i = 0; i < mountPoints.length; i++) {// for all connected parts
                recurseAndAdd(mountPoints[i].connectedPart); // add their CoM to this parts CoM
            }
        }
    }

    recurseAndAdd(body);
    var totalMass = 0;
    var CoM = new THREE.Vector3();
    for (var i = 0; i < parts.length; i++) {
        var part = parts[i];
        totalMass += part.mass;
        var vector = new THREE.Vector3();
        vector.setFromMatrixPosition(part.mesh.matrixWorld);
        CoM.add(vector.multiplyScalar(part.mass));
    }
    CoM.divideScalar(totalMass);
    return CoM;
}

function mmToScene(point) {
    var newPoint = new Point(0, 0, 0);
    newPoint.x = point.x / 100;
    newPoint.y = point.y / 100;
    newPoint.z = point.z / 100;
    return newPoint;
}

function sceneToMM(point) {
    var newPoint = new Point(0, 0, 0);
    newPoint.x = point.x * 100;
    newPoint.y = point.y * 100;
    newPoint.z = point.z * 100;
    return newPoint;
}

var gpNum = 0;
var lastTime = performance.now();
function animate(time) {
    if((time - lastTime) > 16){ // if over 16ms has passed since our last request to the blockMovement Handler then we can request it again.
        lastTime = time;
        if (state === "loaded") { // triggered when robot parts have finished loading
            let gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);

            if (gamepads[0] != null) {
                let gp = gamepads[gpNum];
                if (lastGamepad === undefined) lastGamepad = clone(gamepads[gpNum].axes);

                var cur_sum = 0;
                var last_sum = 0;
                for (var i = 0; i < gp.axes.length; i++) {
                    cur_sum += Math.abs(gp.axes[i]);
                    last_sum += Math.abs(lastGamepad[i]);
                }
                delta = 0.2 * delta + 0.8 * Math.abs(cur_sum - last_sum);
                lastGamepad = clone(gamepads[gpNum].axes);
                // console.log(gp.axes[3]);

                if (gp.buttons[7].pressed === false) {

                    cameraRotation += Math.abs(gp.axes[2]) > 0.05 ? gp.axes[2] / 20 : 0;
                    cameraHeight += Math.abs(gp.axes[3]) > 0.05 ?  -gp.axes[3] / 10 : 0;
                    zoom += Math.abs(gp.axes[1]) > 0.05 ? gp.axes[1] / 10 : 0;
                } else {

                    bodyLocation.z = gp.axes[3] * 40;
                    bodyLocation.x = -gp.axes[1] * 40;
                    bodyLocation.y = -gp.axes[0] * 40;
                    //
                    // body.mesh.position.z = Math.min(1, Math.max(-0.05, gp.axes[3] * 0.6));
                    // if (body.mesh.position.z > -0.05) {
                    //     body.mesh.position.x = -gp.axes[1] * 0.6;
                    //     body.mesh.position.y = -gp.axes[0] * 0.6;
                    // }
                    // var angles = LegIK(new Point(150-(60*gp.axes[1]),0-(150*gp.axes[0]),-25-gp.axes[3]*100));
                    // hip.mesh.rotation.z = angles.gamma;
                    // femur.mesh.rotation.y = angles.a;
                    // foot.mesh.rotation.y = angles.beta;
                    updateIK();
                }

            }
        }
        render();
    }
}

var zoom = 7;

function render() {
    var timer = cameraRotation;//Date.now() * 0.0001;
    camera.position.x = Math.cos(timer) * zoom;
    camera.position.y = Math.sin(timer) * zoom;
    camera.position.z = cameraHeight;
    camera.lookAt(cameraTarget);

    textList.update();
    composer.render(scene, camera);

    requestAnimationFrame(animate);
}


window.addEventListener('wheel', function (e) {
    zoom += e.deltaY / 1000;
});

function degToRad(a) {
    a = a * Math.PI / 180;
    return a;
}

function radToDeg(a) {
    a = a * 180 / Math.PI;
    return a;
}

function LegIK(P) { //robot specific
    var a, gamma, beta;
    var x = P.x;
    var y = P.y;
    var z = -1 * P.z;
    var a1, a2, L1, L;
    gamma = (((Math.atan2(y, x)) * 180 / Math.PI)) * coxiaResolution; // add 180 for servo offset mx12 range 0-360 w/ 180 = center
    L1 = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    L = Math.sqrt(Math.pow((L1 - Coxia), 2) + Math.pow(z, 2));
    if (L > (Femur + Tibia)) { //limit L to the Length of the Femur and Tibia combined, as to not get imaginary results when the foot cant reach the desired position.
        L = Femur + Tibia; // warning could be thrown in here saying cant be reached.
    }
    if ((L1 <= Coxia) || (z >= L)) { //make sure we use the correct one depending on where L is in relation to vertical Z, dont want odd angles
        a1 = Math.asin((L1 - Coxia) / L);
    } else {
        a1 = Math.acos(z / L);
    }
    a2 = Math.acos((Math.pow(Tibia, 2) - Math.pow(Femur, 2) - Math.pow(L, 2)) / (-2 * Femur * L));
    a = -(-90 + 11.33 + ((a1 + a2) * 180 / Math.PI)) * femurResolution; //add 60 cause ax12 0-300 deg w/ 150 = center and add Femur offset cause leg design
    beta = (180 - 78.33 - ((Math.acos((Math.pow(L, 2) - Math.pow(Femur, 2) - Math.pow(Tibia, 2)) / (-2 * Tibia * Femur))) * 180 / Math.PI)) * tibiaResolution; // add 78.33 for leg design offset subtract 30 due to difference between old mx12 180 center and new ax12 150 centers
    //console.log(a,gamma,beta);
    return {a: degToRad(a), gamma: degToRad(gamma), beta: degToRad(beta)};
}

function rotate(p, rotation) { // takes radians
    var newPoint = new Point(0, 0, 0);
    newPoint.x = ((p.x * (Math.cos(rotation.y) * Math.cos(rotation.z))) - (p.y * (Math.cos(rotation.y) * Math.sin(rotation.z))) + (p.z * Math.sin(rotation.y)));
    newPoint.y = ((p.x * (Math.sin(rotation.x) * Math.sin(rotation.y) * Math.cos(rotation.z) + Math.cos(rotation.x) * Math.sin(rotation.z))) + (p.y * (Math.sin(rotation.x) * Math.sin(rotation.y) * Math.sin(rotation.z) + Math.cos(rotation.x) * Math.cos(rotation.z))) - (p.z * (Math.sin(rotation.x) * Math.cos(rotation.y))));
    newPoint.z = ((p.x * -1 * (Math.cos(rotation.x) * Math.sin(rotation.y) * Math.cos(rotation.z) + Math.sin(rotation.x) * Math.sin(rotation.z))) + (p.y * (Math.cos(rotation.x) * Math.sin(rotation.y) * Math.sin(rotation.z) + Math.sin(rotation.x) * Math.cos(rotation.z))) + (p.z * (Math.cos(rotation.x) * Math.cos(rotation.y))));
    return newPoint;
}

//console.log(rotate(rotate({x:100,y:0,z:100},{x:0,y:0,z:1.57}),{x:0,y:0,z:-1.57}));
function clone(obj) {
    if (obj == null || typeof(obj) != 'object')
        return obj;
    let temp = new obj.constructor();
    for (let key in obj) {
        temp[key] = clone(obj[key]);
    }
    return temp;
}
